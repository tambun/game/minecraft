package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var AppConfig conf

type conf struct {
	ServerJAR       string     `yaml:"server_jar"`
	RunArgument     string     `yaml:"run_argument"`
	DiscordBotToken string     `yaml:"discord_bot_token"`
	ChannelID       *channelID `yaml:"channel_id"`
}

type channelID struct {
	Chat    string `yaml:"chat"`
	Command string `yaml:"command"`
}

func InitializeAppConfig(fileName string) (err error) {
	var yamlFile []byte
	if yamlFile, err = ioutil.ReadFile(fileName); err != nil {
		return
	}
	if err = yaml.UnmarshalStrict(yamlFile, &AppConfig); err != nil {
		return
	}

	return
}
