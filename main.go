package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/tambun/game/minecraft/config"
)

var (
	cmd    *exec.Cmd
	stdout io.ReadCloser
	stderr io.ReadCloser
	stdin  io.WriteCloser

	doneRe   = regexp.MustCompile(".+\\[Server thread/INFO]: Done.+")
	fireRe   = regexp.MustCompile(".+Can't keep up! Is the server overloaded\\?.+")
	joinRe   = regexp.MustCompile(".+joined the game.*")
	leaveRe  = regexp.MustCompile(".+left the game.*")
	nameRe   = regexp.MustCompile("[a-zA-Z0-9_]+")
	isChatRe = regexp.MustCompile(".*<[a-zA-Z0-9_]+> .+")
	chatRe   = regexp.MustCompile(">( .+)+$")

	activePlayers = 0
	playerWebhook *discordgo.Webhook
)

func init() {
	var err error
	log.SetPrefix("[TambunMC] ")
	if err = config.InitializeAppConfig("conf.yml"); err != nil {
		log.Fatalf("[InitializeAppConfig] failed reading conf.yml. %+v", err)
	}
	log.Println("Starting...")
}

func main() {
	var err error
	var dg *discordgo.Session
	if dg, err = discordgo.New("Bot " + config.AppConfig.DiscordBotToken); err != nil {
		log.Fatalf("[Discord] Error creating Discord session. %+v", err)
	}

	dg.AddHandler(ready)
	dg.AddHandler(ReceiveCommand)

	if err = dg.Open(); err != nil {
		log.Fatalf("[Discord] Error opening Discord session. %+v", err)
	}

	log.Println("TambunMC is starting up...")
	_ = SendUpDown(dg, &discordgo.MessageEmbed{
		Color:       0xcccc18,
		Timestamp:   time.Now().Format(time.RFC3339),
		Description: "TambunMC is starting up...",
	})
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-c

	log.Printf("Stopping...")
	_ = ResetWebhook(dg)
	if err := cmd.Process.Signal(syscall.SIGINT); err != nil {
		log.Fatalf("[TambunMC] failed to stopping server. %+v ", err)
	}
	_, _ = cmd.Process.Wait()
	_ = SendUpDown(dg, &discordgo.MessageEmbed{
		Color:       0xdd3311,
		Timestamp:   time.Now().Format(time.RFC3339),
		Description: "TambunMC stopping!",
	})
	_ = dg.Close()
}

func ready(s *discordgo.Session, _ *discordgo.Ready) {
	_ = s.UpdateStatus(1, "Minecraft")
	_ = ResetWebhook(s)
	playerWebhook, _ = s.WebhookCreate(config.AppConfig.ChannelID.Chat, "TambunMC WebHook", "")
	args := []string{"-jar", config.AppConfig.ServerJAR, "nogui"}
	args = append(strings.Split(config.AppConfig.RunArgument, " "), args...)
	cmd = exec.Command("java", args...)
	stdout, _ = cmd.StdoutPipe()
	stderr, _ = cmd.StderrPipe()
	stdin, _ = cmd.StdinPipe()
	if err := cmd.Start(); err != nil {
		log.Fatalf("[TambunMC] failed starting server. %+v", err)
	}
	go func() {
		input := bufio.NewReader(stdout)
		defer stdout.Close()
		for {
			response, _ := input.ReadString('\n')
			go log.Print(response)
			_ = ProcessResponse(s, response)
		}
	}()
	go func() {
		input := bufio.NewReader(stderr)
		defer stderr.Close()
		for {
			response, _ := input.ReadString('\n')
			go log.Print(response)
			_ = SendCommand(s, response)
		}
	}()
}

func ReceiveCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID || m.Author.ID == playerWebhook.ID || m.Author.Bot {
		return
	}
	if m.ChannelID == config.AppConfig.ChannelID.Command {
		command := m.Content
		if _, err := io.WriteString(stdin, command+"\n"); err != nil {
			log.Printf("failed writing command: %s. %+v\n", command, err)
		}
		log.Printf("[Discord] Command: `%s`\n", command)
	}
	log.Println(m.ChannelID)
	if m.ChannelID == config.AppConfig.ChannelID.Chat {
		name := m.Author.Username
		chat := m.Content
		log.Println("Chat: " + chat)
		if _, err := io.WriteString(stdin, fmt.Sprintf("/tellraw @a [\"\",{\"text\":\"<Discord: %s> %s\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Chat sent from Discord\"}]}}}]\n", name, chat)); err != nil {
			log.Printf("failed sending chat from %s: %s. %+v\n", name, chat, err)
		}
	}
}

func SendCommand(s *discordgo.Session, msg string) (err error) {
	_, err = s.ChannelMessageSend(config.AppConfig.ChannelID.Command, msg)
	return
}

func SendUpDown(s *discordgo.Session, msg *discordgo.MessageEmbed) (err error) {
	_, err = s.ChannelMessageSendEmbed(config.AppConfig.ChannelID.Chat, msg)
	return
}

func SendChat(s *discordgo.Session, msg string) (err error) {
	_, err = s.ChannelMessageSend(config.AppConfig.ChannelID.Chat, msg)
	return
}

func ProcessResponse(s *discordgo.Session, resp string) (err error) {
	switch {
	case doneRe.MatchString(resp):
		execTimeRe := regexp.MustCompile("\\d+\\.\\d+.")
		_ = SendUpDown(s, &discordgo.MessageEmbed{
			Color:       0x2255cc,
			Timestamp:   time.Now().Format(time.RFC3339),
			Description: fmt.Sprintf("TambunMC is running!\nStarting up took `%s`", execTimeRe.FindString(resp)),
		})
	case fireRe.MatchString(resp):
		_ = SendChat(s, fmt.Sprintf("`%s` 🔥 Server is ***HOT*** 🔥", time.Now().Format(time.RFC822)))
	case joinRe.MatchString(resp):
		activePlayers++
		name := GetPlayerName(resp)
		_ = SendChat(s, fmt.Sprintf("🔼 **%s** joined the game! %d player%s in game.", name, activePlayers, (map[bool]string{true: "", false: "s"})[activePlayers == 1]))
		_ = UpdatePlayerStatus(s)
	case leaveRe.MatchString(resp):
		activePlayers--
		name := GetPlayerName(resp)
		_ = SendChat(s, fmt.Sprintf("🔽 **%s** left the game! %d player%s in game.", name, activePlayers, (map[bool]string{true: "", false: "s"})[activePlayers == 1]))
		_ = UpdatePlayerStatus(s)
	case isChatRe.MatchString(resp):
		name := GetPlayerName(resp)
		chat := chatRe.FindString(strings.TrimSpace(resp))[2:]
		log.Println(chat)
		jsonBody, _ := json.Marshal(map[string]string{"username": name, "content": chat})
		_, _ = http.Post(fmt.Sprintf("https://discordapp.com/api/webhooks/%s/%s", playerWebhook.ID, playerWebhook.Token), "application/json", bytes.NewBuffer(jsonBody))
	}
	_ = SendCommand(s, resp)
	return
}

func UpdatePlayerStatus(s *discordgo.Session) (err error) {
	if _, err = s.ChannelEditComplex(config.AppConfig.ChannelID.Chat, &discordgo.ChannelEdit{
		Topic: fmt.Sprintf("%d %s playing!", activePlayers, (map[bool]string{true: "person is", false: "people are"}[activePlayers == 1])),
	}); err != nil {
		return
	}
	if activePlayers == 0 {
		return s.UpdateStatus(1, "")
	} else {
		return s.UpdateStatus(0, "Minecraft")
	}
}

func ResetWebhook(s *discordgo.Session) (err error) {
	webhooks, _ := s.ChannelWebhooks(config.AppConfig.ChannelID.Chat)
	for _, wh := range webhooks {
		if err = s.WebhookDelete(wh.ID); err != nil {
			return
		}
	}

	return
}

func GetPlayerName(resp string) string {
	match := nameRe.FindAllString(resp, 7)
	return match[len(match)-1]
}
