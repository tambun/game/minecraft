FROM golang:1.12 AS builder

RUN apt-get update -qq && apt-get install -qq -y ca-certificates
RUN go get -u github.com/golang/dep/cmd/dep

WORKDIR $GOPATH/src/gitlab.com/tambun/game/minecraft
COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure --vendor-only
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /tambunmc .

FROM openjdk:8-alpine
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /tambunmc /app/
WORKDIR /minecraft

EXPOSE 25565

ENTRYPOINT ["/app/tambunmc"]
